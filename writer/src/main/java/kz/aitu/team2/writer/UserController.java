package kz.aitu.team2.writer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class UserController {
    @Autowired
    private kz.aitu.team2.writer.UserRepository userRepository;

    @GetMapping(path="/add-user")
    public @ResponseBody String addNewUser (@RequestParam(value = "name") String name) throws InterruptedException {
        kz.aitu.team2.writer.User n = new kz.aitu.team2.writer.User();
        n.setName(name);
        userRepository.save(n);
//        Thread.sleep(200);
        return String.format("Hello %s!", name);
    }

}
