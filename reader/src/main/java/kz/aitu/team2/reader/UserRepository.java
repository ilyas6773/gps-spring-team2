package kz.aitu.team2.reader;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface UserRepository extends JpaRepository<kz.aitu.team2.reader.User, Long> {

    List<kz.aitu.team2.reader.User> findAll();
}
