package kz.aitu.team2.reader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class UserController {
    @Autowired
    private kz.aitu.team2.reader.UserRepository userRepository;

    @GetMapping(path="/get-names")
    public @ResponseBody Iterable<kz.aitu.team2.reader.User> getAllUsers() {
        return userRepository.findAll();
    }

}
